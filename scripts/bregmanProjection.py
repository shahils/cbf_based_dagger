import numpy as np
from scipy.optimize import minimize

def bergman_projection(p, g, x0):
    """
    Finds the Bergman projection of the function f onto the set defined by the inequality constraint g(x) <= 0.
    
    Parameters:
    - f: Function to be projected
    - g: Constraint function (inequality)
    - x0: Initial guess for the projection
    
    Returns:
    - x_proj: Bergman projection of f onto the set defined by g(x) <= 0
    """
    
    def objective(x):
      return np.linalg.norm(x - p)
    
    def constraint(x):
      print('x = ', x)
      return g(x)
    
    bounds = None  # If you have bounds for the variables, you can specify them here
    
    # Solve the optimization problem
    res = minimize(objective, x0, constraints={'type': 'ineq', 'fun': constraint}, bounds=bounds)
    
    if res.success:
      x_proj = res.x
      return x_proj
    else:
      raise ValueError("Optimization failed. Could not find the Bergman projection.")

# Example usage:

# Define the function to be projected (2D example)
'''def f(x):
    return np.array([x[0]**2 + x[1]**2 - 1, x[0] + x[1] - 2])'''

# Define the point to be projected (2D example)
p = np.array([15, 50])

# Define the constraint function (inequality)
def g(x):
  return [x[0] - 10, x[1] - 5]
  #return [-1, -2]

# Initial guess for the projection
x0 = np.array([0.1, 0.5])

# Find the Bergman projection
x_proj = bergman_projection(p, g, x0)

print("Bergman projection:", x_proj)
