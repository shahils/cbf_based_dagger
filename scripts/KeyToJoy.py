# used to convert joystick command to cmd_vel rostopic
import rospy
import numpy as np
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist

class KeyToJoy():
    def __init__(self):
        rospy.init_node('KeyToJoy', log_level=rospy.DEBUG)
        self.sub = rospy.Subscriber('/joy', Joy, self.joy_input)
        self.pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        global roam 
        roam = Twist()

    def joy_input(self,msg):
        roam.linear.x = msg.axes[1] * 6
        roam.angular.z = msg.axes[0] * 4

        self.pub.publish(roam)

if __name__ == '__main__':
	KeyToJoy()
	rospy.spin()
