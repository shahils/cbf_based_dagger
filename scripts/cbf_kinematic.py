#! /usr/bin/env python3

import rospy
import numpy as np
import math
import time
import tf
import tf.msg
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Joy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Polygon, PolygonStamped, Point32
from std_msgs.msg import Header
from jsk_recognition_msgs.msg import PolygonArray
from jsk_rviz_plugins.msg import OverlayText
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from functools import partial
from std_msgs.msg import ColorRGBA

from bregmanProjectionCBF import bregmanProjection

global u1

class CBF():

	def __init__(self):
		
		global roam			
		rospy.init_node('CBF_Function', log_level=rospy.DEBUG)

		self.input = Twist()
		roam = Twist()

		# Subscribers if you are working inside phoenix stack
		'''self.odom_sub = rospy.Subscriber('/warty/odom', Odometry, self.odom_callback)
		self.sub = rospy.Subscriber('/joy', Joy, self.joy_input)
		self.sub = rospy.Subscriber('/warty/laser_scan', LaserScan, self.lidar_data)
		self.sub = rospy.Subscriber('/warty/warthog_velocity_controller/cmd_vel_actual', Twist, self.phoenix_vel)
		self.pub_u = rospy.Publisher('/safe_cmd_vel', Twist, queue_size=1)'''

		# Subscribers if you are working outside phoenix stack
		self.odom_sub = rospy.Subscriber('/unity_odom_info', Odometry, self.odom_callback)
		self.sub = rospy.Subscriber('/joy', Joy, self.joy_input)
		self.sub = rospy.Subscriber('/laser_scan', LaserScan, self.lidar_data)
		#self.sub = rospy.Subscriber('/cmd_vel', Twist, self.phoenix_vel)
		
		#self.pub = rospy.Publisher('/warty/warthog_velocity_controller/cmd_vel', Twist, queue_size=1)
		self.pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
		self.text_pub = rospy.Publisher("/warning_text", OverlayText, queue_size=1)
		self.pub_poly_front = rospy.Publisher('/polygon_front', PolygonStamped, queue_size=1)
		self.pub_poly_rear = rospy.Publisher('/polygon_rear', PolygonStamped, queue_size=1)
		
		self.t_min = 0.5																												# minimum look ahead time
		self.m = 16.523																													# mass of robot
		self.d_long = 30																												# damping coefficient
		self.d_lat = 110																												# damping coefficient
		self.R = 0.098																													# radius of wheel
		self.mu = 0.7																													# coefficient of friction
		self.g = 9.81																													# gravity
		self.D_e_e = np.zeros([3,1])																									# external force acting on ego vehicle in body frame
		self.D_s_s = np.zeros([3,1])																									# external force acting on surrounding vehicle in body frame
		self.vb_s = np.array([[0],[0],[0]])																								# surrounding vehicle velocity in body frame
		self.position_s = np.array([[0],[0],[0]])																						# surrounding vehicle position wrt origin
		self.orientation_s = np.array([[0],[0],[0]])																					# surrounding vehicle orientation
		self.clearence = 2																											# minimum required distance for safety
		self.angle = 0																													# angle of the obstacle wrt ego vehicle frame
		self.lin_vel_e_b = np.array([[0],[0],[0]])																						# ego vehicle velocity in body frame
		self.ang_vel_e_b = np.array([[0],[0],[0]])																						# ego vehicle angular velocity in body frame
		self.position_e = np.array([[0],[0],[0]])																						# ego vehicle position wrt origin
		self.orientation_e = [[0],[0],[0]]																								# ego vehicle orientation wrt origin
		self.u = np.zeros([2,1])																										# input to the robot
		self.scan = np.zeros([1,723])																									# initialize lidar readings
		self.input.linear = 0																											# initialize joy linear
		self.input.angular = 0																											# initialize joy angular
		self.Ae = np.array([[1, 0], [0, 0], [0, 0]])
		self.Be = np.array([0, 1])
		self.obs_distance = 10																											# initialize distance between robot and obstacle
		self.h = 0																														# initialize cbf value
		self.phx_linear_vel = 0																											# initialize 0 linear velocity from phoenix
		self.phx_angular_vel = 0																										# initialize 0 angular velocity from phoenix
		#self.u1 = 0
		
	def lidar_data(self,msg):
		self.scan[0,:] = np.array(msg.ranges)
		
	def phoenix_vel(self,msg):
		self.phx_linear_vel = msg.linear.x
		self.phx_angular_vel = msg.angular.z
		
	def joy_input(self,msg):
		# Reading and acaling joy stick input
		self.input.linear = msg.axes[1] * 5
		self.input.angular = msg.axes[0] * 3
		
	def odom_callback(self,msg):
		# Velocity of ego vehicle from odometry
		self.lin_vel_e_b = np.array([[msg.twist.twist.linear.x],[msg.twist.twist.linear.y],[msg.twist.twist.linear.z]])
		self.ang_vel_e_b = np.array([[msg.twist.twist.angular.x],[msg.twist.twist.angular.y],[msg.twist.twist.angular.z]])
		
		# Position of ego vehicle from odometry
		self.position_e = np.array([[msg.pose.pose.position.x],[msg.pose.pose.position.y],[msg.pose.pose.position.z]])
		self.orientation_e = self.quaternions_2_ypr(msg.pose.pose.orientation)
		
		# Figure of the obstacle position
		self.position_s, self.angle = self.obstacle_position()
		self.vel_cbf()
		self.safe_set()
		self.safeControl()
		
	def quaternions_2_ypr(self,quats):	
		# Converts quartenions to euiler angles
		x = quats.x
		y = quats.y
		z = quats.z
		w = quats.w
		
		yaw = math.atan2(2*(x*y + w*z), w**2+x**2-y**2-z**2)
		pitch = 0
		roll = 0
		
		return [[roll],[pitch],[yaw]]																									# Tested and Working
		
	def transform(self,orientation):
		
		phi = orientation[0][0]
		theta = orientation[1][0]
		psi = orientation[2][0]
		
		L_x = np.array([[1,0,0],[0,math.cos(phi),-math.sin(phi)],[0,math.sin(phi),math.cos(phi)]])
		L_y = np.array([[math.cos(theta),0,math.sin(theta)],[0,1,0],[-math.sin(theta),0,math.cos(theta)]])
		L_z = np.array([[math.cos(psi),-math.sin(psi),0],[math.sin(psi),math.cos(psi),0],[0,0,1]])
		
		L_bi = L_x.dot(L_y.dot(L_z))																									# transformation from inertial to body frame

		return L_bi																														# Tested and working
	
	def obstacle_position(self):
		L_oe = self.transform(self.orientation_e)																						# transform from inertial to ego vehicle frame
		L_eo = np.linalg.inv(L_oe)																										# transform from ego vehicle to inertial frame
		
		self.obs_distance = np.min(self.scan)
		idx = np.array(np.where(self.scan == self.obs_distance))
		angle = (2*math.pi/723)*idx[1][0]
		
		#if angle > math.pi:
		angle = -angle + math.pi
		
		es_e = [[self.obs_distance * math.cos(angle)], [self.obs_distance * math.sin(angle)], [0]]										# obstacle position wrt ego vehicle
		es_o = L_oe.dot(es_e)																											# obstacle position transformed to odom frame
		
		xs = self.position_e + es_o																										# obstacle position wrt to odom frame
		
		return xs, angle																												
		
	def vel_cbf(self):
		
		L_oe = self.transform(self.orientation_e)																						# transform from inertial to ego vehicle frame
		L_eo = np.linalg.inv(L_oe)																										# transform from ego vehicle to inertial frame
		L_os = self.transform(self.orientation_s)																						# transform from inertial to ego vehicle frame
		L_so = np.linalg.inv(L_os)																										# transform from ego vehicle to inertial frame
		
		x1_dot = self.lin_vel_e_b[0][0]																									# lin vel of ego vehicle in odom frame
		x2 = self.angle																													# angle between robot heading and obstacle
		x3 = self.obs_distance																											# distance between ego vehicle and obstacle
		x3_dot = np.dot(L_os,self.vb_s) - np.dot(L_oe,self.lin_vel_e_b)																	# difference between obstacle and ego vehicle velocities
		
		self.h = x3 - self.clearence - self.t_min*x1_dot*math.cos(x2) 																	# (np.linalg.norm(x2)**2)/(2*self.mu*self.g)

		return self.h																													

	def del_vel_cbf(self):
		
		L_oe = self.transform(self.orientation_e)																						# transform from inertial to ego vehicle frame
		L_eo = np.linalg.inv(L_oe)																										# transform from ego vehicle to inertial frame
		L_os = self.transform(self.orientation_s)																						# transform from inertial to ego vehicle frame
		L_so = np.linalg.inv(L_os)																										# transform from ego vehicle to inertial frame
		
		x1_dot = self.lin_vel_e_b																										# lin vel of ego vehicle in odom frame
		x2 = self.angle																													# angle between robot heading and obstacle
		x3 = self.obs_distance																											# distance between ego vehicle and obstacle
		#x3_dot = np.dot(L_os,self.vb_s) - np.dot(L_oe,self.lin_vel_e_b)																# difference between obstacle and ego vehicle velocities
		x3_dot = self.lin_vel_e_b[0][0]
		
		grad_h = np.zeros([1,7])
		
		grad_h[0,3] = self.t_min*x3_dot*math.sin(x2)
		
		if np.linalg.norm(x3) != 0:
			grad_h[0,4:7] = self.t_min*np.transpose(x3)/(np.linalg.norm(x3))
			
		if np.linalg.norm(x3) == 0:
			grad_h[0,4:7] = self.t_min*np.transpose(x3)
		
		f_x = np.zeros([7,1])
		
		g_x = np.zeros([7,2])
		g_x[0:3,:] = L_oe.dot(self.Ae)
		g_x[3,:] = self.Be
		g_x[4:7,:] = -L_oe.dot(self.Ae)
		
		Lf_h = grad_h.dot(f_x)
		Lg_h = grad_h.dot(g_x)

		return Lf_h, Lg_h																												# Tested and works fine
    
	# Function to draw the safe space around robot. Can be visualized through Rviz
	def safe_set(self):
		
		global u_safe
		L_oe = self.transform(self.orientation_e)																						# transform from inertial to ego vehicle frame
		L_eo = np.linalg.inv(L_oe)																										# transform from ego vehicle to inertial frame
		Lf_h, Lg_h = self.del_vel_cbf()
		
		p_front = PolygonStamped()
		p_front.header.frame_id = "Warthog"
		p_front.polygon.points = [Point32(x = 0, y = 0)]
		
		p_rear = PolygonStamped()
		p_rear.header.frame_id = "Warthog"
		p_rear.polygon.points = [Point32(x = 0, y = 0)]
		
		u2 = np.linspace(-1, 1, num=50)
		
		print('CBF value = ', self.h)
		
		for i in range (50):
			
			u1_safe = (-self.h**2 - Lg_h[0][1]*u2[i])/Lg_h[0][0]
			
			if Lg_h[0][0] > 0:
				#print('the safe set is u1 >= ', u1_safe)
				if u1_safe < 0:
					eta = u2[i] * 1
					x = 3 * 1 * math.cos(eta)
					y = 3 * 1 * math.sin(eta)
					p_front.polygon.points.append(Point32(x = x, y = y))
					
					if u1_safe > -3 and u1_safe < 3:
						eta = u2[i] * 1
						x = u1_safe * 1 * math.cos(-eta)
						y = u1_safe * 1 * math.sin(-eta)
						p_rear.polygon.points.append(Point32(x = x, y = y))
					 		
					else:
						eta = u2[i] * 1
						x = -3 * 1 * math.cos(-eta)
						y = -3 * 1 * math.sin(-eta)
						p_rear.polygon.points.append(Point32(x = x, y = y))
				else:
					eta = u2[i] * 1
					x = 3 * 1 * math.cos(eta)
					y = 3 * 1 * math.sin(eta)
					p_front.polygon.points.append(Point32(x = x, y = y))
					p_rear.polygon.points.append(Point32(x = 0, y = 0))
				 	
			elif Lg_h[0][0] < 0:
				#print('the safe set is u1 <= ', u1_safe)
				if u1_safe > 0:
					eta = u2[i] * 1
					x = -3 * 1 * math.cos(eta)
					y = -3 * 1 * math.sin(eta)
					p_rear.polygon.points.append(Point32(x = x, y = y))
				
					if u1_safe > -3 and u1_safe < 3:
						eta = u2[i] * 1
						x = u1_safe * 1 * math.cos(eta)
						y = u1_safe * 1 * math.sin(eta)
						p_front.polygon.points.append(Point32(x = x, y = y))
								
					else:
						eta = u2[i] * 1
						x = 3 * 1 * math.cos(eta)
						y = 3 * 1 * math.sin(eta)
						p_front.polygon.points.append(Point32(x = x, y = y))	
				else:
					eta = u2[i] * 1
					x = -3 * 1 * math.cos(eta)
					y = -3 * 1 * math.sin(eta)
					p_rear.polygon.points.append(Point32(x = x, y = y))
					p_front.polygon.points.append(Point32(x = 0, y = 0))
					
			else:
				print('Still waiting for a valid safe region')
				eta = u2[i] * 1
				x = 3 * 1 * math.cos(eta)
				y = 3 * 1 * math.sin(eta)
				p_front.polygon.points.append(Point32(x = x, y = y))
				x = -3 * 1 * math.cos(eta)
				y = -3 * 1 * math.sin(eta)
				p_rear.polygon.points.append(Point32(x = x, y = y))

			self.pub_poly_front.publish(p_front)
			self.pub_poly_rear.publish(p_rear)
				
		return u1_safe
    
	# Function to verify safety using CBF constraint
	def safeCheck(self, Lg_h):
		u = [self.input.linear, self.input.angular]
		cbfConstraint = u[0]*Lg_h[0][0] + Lg_h[0][1]*u[1] + self.h

		if cbfConstraint >= 0:
			isSafe = True
		else:
			isSafe = False
		
		return isSafe
    
	# Function to implement safe control
	def safeControl(self):
		Lf_h, Lg_h = self.del_vel_cbf()
		isSafe = self.safeCheck(Lg_h)															# Verify safety using CBF constraint
		print('is safe = ', isSafe)
        
		# if action is safe, implement it on to the robot
		if isSafe:
			roam.linear.x = self.input.linear
			roam.angular.z = self.input.angular

		# else find the bregman projection of the intended action and then implement it on to the robot
		else:
			bp = bregmanProjection(self.h, Lg_h, [self.input.linear, self.input.angular])
			u_proj = bp.bergman_projection()
			roam.linear.x = u_proj[0]
			roam.angular.z = u_proj[1]
			
		self.pub.publish(roam)

		'''u1_safe = (-self.h**2 - Lg_h[0][1]*self.input.angular)/Lg_h[0][0]

		if Lg_h[0][0] > 0:
			print('the safe input is u > ', u1_safe)
		elif Lg_h[0][0] < 0:
			print('the safe input is u < ', u1_safe)
		else:
			print('Still waiting for a valid safe region')'''


if __name__ == '__main__':
	
	CBF()
	rospy.spin()
