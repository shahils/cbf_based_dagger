#! /usr/bin/env python3

import tensorflow.compat.v1 as tf1
import rospy
import numpy as np
import math
import time
import tf
#import tensorflow as tf1
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Joy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Polygon, PolygonStamped, Point32, PoseStamped
from std_msgs.msg import Header
#from jsk_recognition_msgs.msg import PolygonArray
#from jsk_rviz_plugins.msg import OverlayText
#import matplotlib.pyplot as plt
#from matplotlib.animation import FuncAnimation
from functools import partial
from std_msgs.msg import ColorRGBA
from visualization_msgs.msg import InteractiveMarkerInit

global u1

tf1.compat.v1.disable_eager_execution()
physical_devices = tf1.config.list_physical_devices('GPU') 
for device in physical_devices:
	tf1.config.experimental.set_memory_growth(device, True)

class human_control():
	def __init__(self):
		global roam			
		rospy.init_node('Pure_Human', log_level=rospy.DEBUG)

		self.input = Twist()
		roam = Twist()
		
		##### Used inside stack
		'''self.sub = rospy.Subscriber('/rviz/team_behavior_graph/update_full', InteractiveMarkerInit, self.target_position)
		self.odom_sub = rospy.Subscriber('/warty/warthog_velocity_controller/odom', Odometry, self.odom_callback)
		self.sub = rospy.Subscriber('/joy', Joy, self.joy_input)
		self.sub = rospy.Subscriber('/laser_scan', LaserScan, self.lidar_data)
		self.pub = rospy.Publisher('/warty/warthog_velocity_controller/cmd_vel', Twist, queue_size=1)'''
		
		##### Used outside stack
		self.sub = rospy.Subscriber('/goal_pose', Odometry, self.target_position)
		self.odom_sub = rospy.Subscriber('/unity_odom_info', Odometry, self.odom_callback)
		self.sub = rospy.Subscriber('/joy', Joy, self.joy_input)
		self.sub = rospy.Subscriber('/laser_scan', LaserScan, self.lidar_data)
		self.pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
		
		self.lin_vel_e_b = np.array([[0],[0],[0]])		# ego vehicle velocity in body frame
		self.angular_vel_e_b = np.array([[0],[0],[0]])
		self.ang_vel_e_b = np.array([[0],[0],[0]])		# ego vehicle angular velocity in body frame
		self.position_e = np.array([[0],[0],[0]])		# ego vehicle position wrt origin
		self.orientation_e = [[0],[0],[0]]			# ego vehicle orientation wrt origin
		self.scan = np.zeros([1,723])				# initialize lidar readings
		self.input.linear = 0					# initialize joy linear
		self.input.angular = 0					# initialize joy angular
		self.goal_x = 0
		self.goal_y = 0
		self.switch = 0
		self.count = 0	
		self.autonomy = 0
		self.sess = tf1.Session()
		self.S1 = tf1.placeholder(tf1.float32, [None, 5], 'S1')
		self.S2 = tf1.placeholder(tf1.float32, [None, 724], 'S2')
		self.keep_prob = tf1.placeholder(tf1.float32)
		self.a_predict = tf1.placeholder(tf1.float32, [None, 2])
		self.a_predict = self.build_c(self.S1,self.S2,self.keep_prob)
		self.loader()
		self.listener = tf.TransformListener()
		
	##### Used inside stack
	'''def target_position(self,msg):
		for marker in msg.markers:
			self.goal_x = marker.pose.position.x
			self.goal_y = marker.pose.position.y
		self.switch = 0	
		#print(self.goal_x)'''
		
	##### Used outside stack
	def target_position(self,msg):
		self.goal_x = msg.pose.pose.position.x
		self.goal_y = msg.pose.pose.position.y
		#print(self.goal_x)
		
	def lidar_data(self,msg):
		self.scan[0,:] = np.array(msg.ranges)
		#a = self.scan.shaperansform between frames
		#rospy.loginfo("scan = {a}".format(a = a))
		
	def joy_input(self,msg):
		self.input.linear = msg.axes[1] * 5
		self.input.angular = msg.axes[0] * 3
		
		switch = msg.buttons[1]
		if switch == 1:
			self.autonomy = 1
			print('Autonomy')
			
		store = msg.buttons[2]
		if store == 1:
			self.store_data()
		
		clear = msg.buttons[0]
		if clear == 1:
			self.clear_data()
	
	##### Used inside stack
	'''def odom_callback(self,msg):
		# Velocity of ego vehicle from odometry
		self.lin_vel_e_b = np.array([[msg.twist.twist.linear.x],[msg.twist.twist.linear.y],[msg.twist.twist.linear.z]])
		self.ang_vel_e_b = np.array([[msg.twist.twist.angular.x],[msg.twist.twist.angular.y],[msg.twist.twist.angular.z]])
		
		# Position of ego vehicle from odometry
		#position_odom = np.array([[msg.pose.pose.position.x],[msg.pose.pose.position.y],[msg.pose.pose.position.z]])
		#orientation_odom = np.array([[msg.pose.pose.orientation.x],[msg.pose.pose.orientation.y],[msg.pose.pose.orientation.z],[msg.pose.pose.orientation.w]])
		#orientation_odom = self.quaternions_2_ypr(orientation_odom)
		
		#print(orientation_odom)
		#orientation_odom = self.quaternions_2_ypr(msg.pose.pose.orientation)
		
		#self.position_e, self.orientation_e = self.world_odom(position_odom, orientation_odom)
		self.position_e, self.orientation_e = self.world_odom()
		
		self.data_collection()
		self.human_teleop()'''
	
	##### Used outside stack
	def odom_callback(self,msg):
		# Velocity of ego vehicle from odometry
		self.lin_vel_e_b = np.array([[msg.twist.twist.linear.x],[msg.twist.twist.linear.y],[msg.twist.twist.linear.z]])
		self.ang_vel_e_b = np.array([[msg.twist.twist.angular.x],[msg.twist.twist.angular.y],[self.input.angular]])
		
		# Position of ego vehicle from odometry
		self.position_e = np.array([[msg.pose.pose.position.x],[msg.pose.pose.position.y],[msg.pose.pose.position.z]])
		orientation_odom = np.array([[msg.pose.pose.orientation.x],[msg.pose.pose.orientation.y],[msg.pose.pose.orientation.z],[msg.pose.pose.orientation.w]])
		yaw = self.quaternions_2_ypr(orientation_odom)
		self.orientation_e = [[0],[0],[yaw]]
		#self.orientation_e = self.quaternions_2_ypr(orientation_odom)
		
		self.data_collection()
		self.human_teleop()
		
	##### Used outside stack
	'''def orientation(self):
		(trans,rot) = self.listener.lookupTransform('/map', '/husky_base_link', rospy.Time(0))
		rot = np.array(rot)
		yaw = self.quaternions_2_ypr(rot)
		
		orientation = [[0],[0],[yaw]]
		return orientation'''
		
	def quaternions_2_ypr(self,rot):	
	
		# Converts quartenions to euiler angles
		x = rot[0]
		y = rot[1]
		z = rot[2]
		w = rot[3]
		
		yaw = math.atan2(2*(x*y + w*z), w**2+x**2-y**2-z**2)
		pitch = 0
		roll = 0
		
		#print('yaw angle = ', yaw * 180 / math.pi)
		#rospy.logerr("Lg_h = {yaw}".format(yaw=yaw))
		
		return yaw

	##### Used inside stack
	#def world_odom(self, position_odom, orientation_odom):
	'''def world_odom(self):
		(trans,rot) = self.listener.lookupTransform('warty/map', 'warty/base', rospy.Time(0))
		rot = np.array(rot)
		yaw = self.quaternions_2_ypr(rot)
		
		trans = np.array([[trans[0]],[trans[1]],[trans[2]]])
		#position_world = trans + np.dot(np.transpose(rot_matrix), position_odom)
		
		del_x = self.goal_x - trans[0][0]
		del_y = self.goal_y - trans[1][0]
		
		orientation = [[0],[0],[yaw]]
		return trans, orientation'''
		
	def loader(self):
		loader= tf1.train.Saver()
		#loader.restore(self.sess,tf1.train.latest_checkpoint('Human_model_ros_full'))

		
	def build_c(self,S1,S2,keep_prob):
	
		net1 = tf1.layers.dense(S2, 1024, activation=tf1.nn.relu, name='net1', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		drop_out1 = tf1.nn.dropout(net1, keep_prob)
		net2 = tf1.layers.dense(drop_out1, 1024, activation=tf1.nn.relu, name='net2', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		drop_out2 = tf1.nn.dropout(net2, keep_prob)
		net3 = tf1.layers.dense(drop_out2, 512, activation=tf1.nn.relu, name='net3', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		drop_out3 = tf1.nn.dropout(net3, keep_prob)
		net4 = tf1.layers.dense(drop_out3, 512, activation=tf1.nn.relu, name='net4', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		drop_out4 = tf1.nn.dropout(net4, keep_prob)
		net5 = tf1.layers.dense(drop_out4, 256, activation=tf1.nn.relu, name='net5', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		drop_out5 = tf1.nn.dropout(net5, keep_prob)
		net6 = tf1.layers.dense(drop_out5, 128, activation=tf1.nn.relu, name='net6', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		drop_out6 = tf1.nn.dropout(net6, keep_prob)
		net7 = tf1.layers.dense(drop_out6, 64,trainable=True,activation=tf1.nn.relu, name='net7', kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		#drop_out7 = tf1.nn.dropout(net7, keep_prob)
		
		net8 = tf1.layers.dense(S1, 256, activation=tf1.nn.relu, name='net8', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		#drop_out8 = tf1.nn.dropout(net8, keep_prob)
		net9 = tf1.layers.dense(net8,256, activation=tf1.nn.relu,name='net9',trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		#drop_out9 = tf1.nn.dropout(net9, keep_prob)
		net11 = tf1.layers.dense(net9,128, activation=tf1.nn.relu,name='net11',trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		#drop_out11 = tf1.nn.dropout(net11, keep_prob)

		net13_input = tf1.concat([net11, net7], 1)
		net13 = tf1.layers.dense(net13_input, 256, activation=tf1.nn.relu, name='net13', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
		net14 = tf1.layers.dense(net13, 128, activation=tf1.nn.relu, name='net14', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.0003))
	
		a = tf1.layers.dense(net14,2,trainable=True,activation='tanh')
		#a = tf1.layers.dense(net12,2,trainable=True,activation=tf1.tanh)
		#print('output = ', a)
		return tf1.multiply(a, [2,1], name='scaled_a')
		
	def human_model_action(self):
		states = np.zeros([1,5])
		lidar = np.zeros([1,724])
		del_x = self.goal_x - self.position_e[0][0]
		del_y = self.goal_y - self.position_e[1][0]
		states[:5] = self.data_needed[:5]
		#states[:5] = np.array([self.orientation_e[2][0], self.lin_vel_e_b[0][0], self.angular_vel_e_b[2][0], del_x, del_y])
		lidar[:724] = self.data_needed[5:-2]
		a = self.sess.run(self.a_predict, {self.S1:states,self.S2:lidar,self.keep_prob:1})
		#print("original a:",a)
		#return a
		return a[0]
		
	def human_teleop(self):		
		# human_model_action = self.human_model_action()
		
		#print('position = ', self.position_e)
		del_x = self.goal_x - self.position_e[0][0]
		del_y = self.goal_y - self.position_e[1][0]
		
		if self.input.linear != 0 or self.input.angular != 0:
			self.switch = 1
		
		if abs(del_x) > 0.1 and abs(del_y) > 0.1:
			if self.autonomy == 1:
			    roam.linear.x = human_model_action[0]
				roam.angular.z = human_model_action[1]
			else:
				roam.linear.x = self.input.linear
				roam.angular.z = self.input.angular
			self.pub.publish(roam)
				
		else:
			roam.linear.x = self.input.linear	
			roam.angular.z = self.input.angular
		self.pub.publish(roam)

	def data_collection(self):
		del_x = self.goal_x - self.position_e[0][0]
		del_y = self.goal_y - self.position_e[1][0]
		scan = self.scan[0,:]
		
		eta = math.atan2(del_y,del_x)
		
		self.data_needed = np.array([self.orientation_e[2][0], self.lin_vel_e_b[0][0], self.ang_vel_e_b[2][0], del_x, del_y])
		self.data_needed = np.concatenate((self.data_needed,scan),axis=0)
		self.data_needed = np.append(self.data_needed,self.scan[0,0])
		self.data_needed = np.append(self.data_needed,(self.input.linear))
		self.data_needed = np.append(self.data_needed,(self.input.angular))
		#print('del x = ', del_x, 'del y = ', del_y)
		
		if self.goal_x != 0 and self.goal_y != 0:
			if self.input.linear != 0 or self.input.angular != 0:
				#print('DATA IS BEING COLLECTED')
				self.autonomy = 0
				print('del x = ', del_x, 'del y = ', del_y)
				with open("temp_store.dat", "a", newline='') as f:
					f.write(str(self.data_needed).replace('\n','').replace('[','').replace(']','')+'\n')
		
	def store_data(self):
		with open('temp_store.dat','r+') as firstfile, open('pure_human_ros_full.dat','a') as secondfile:
			for line in firstfile:               
				secondfile.write(line)
			firstfile.truncate(0)
		print('Done Storing Data')
		
	def clear_data(self):
		with open('temp_store.dat','r+') as trashfile:
			trashfile.truncate(0)
		print('Cleared Temporary Data')

if __name__ == '__main__':
	human_control()
	rospy.spin()

