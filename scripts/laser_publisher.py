#! /usr/bin/env python3

import rospy
import numpy as np
from sensor_msgs.msg import LaserScan

class laser_publisher():
	def __init__(self):
		global roam			
		rospy.init_node('Publisher_node', log_level=rospy.DEBUG)
		self.sub = rospy.Subscriber('/laser_scan', LaserScan, self.lidar_pub)
		self.scan_pub = rospy.Publisher('/laser_scan_real', LaserScan, queue_size=1)
		self.scan = LaserScan()
		
	def lidar_pub(self,msg):
		self.laser_scan = np.array(msg.ranges)
		self.laser_scan = np.flip(self.laser_scan)
		
		self.scan.header.stamp = rospy.Time.now()
		self.scan.header.frame_id = "Husky"
		self.scan.angle_min = -3.1415
		self.scan.angle_max = 3.1415
		self.scan.angle_increment = 0.0087
		self.scan.time_increment = 1
		self.scan.range_min = 0.1
		self.scan.range_max = 30
		self.scan.ranges = self.laser_scan
		self.scan.intensities = []
		
		self.scan_pub.publish(self.scan)
		
if __name__ == '__main__':
	laser_publisher()
	rospy.spin()
