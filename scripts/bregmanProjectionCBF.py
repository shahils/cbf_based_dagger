import numpy as np
from scipy.optimize import minimize
import math

class bregmanProjection():
  """
    Finds the Bergman projection of the point P onto the set defined by the inequality constraint g(x) <= 0.
    
    Parameters:
    - p: Point to be projected
    - g: CBF Constraint function (inequality)
    - u0: Initial guess for the projection
    
    Returns:
    - x_proj: Bergman projection of p onto the set defined by g(u) <= 0
  """

  def __init__(self, h, Lg_h, p):
    self.Lg_h = Lg_h                                # Used in CBF
    self.d = 1e-6                                   # Define the step size for finite differences
    self.p = p                                      # p is the human input command which is to be projected
    self.u0 = np.array([0,0])                       # Define an universal inital point
    self.u = np.array([0,0])                        # Initialize the variable u
    self.h = h                                      # receive the CBF value

  def bergman_projection(self):
      # Let's define the Bergman Projection equation as an objective function to be minimized
      def objective(u):
        g_of_u = self.f(u)
        g_of_p = self.f(self.p)
        grad_g_of_p = self.grad(self.p)
        return np.linalg.norm(u-self.p)
      
      # Constraint is the CBF function
      def constraint(u):
        return self.g(u)
      
      bounds = [(-5, 5), (-3, 3)]  # If you have bounds for the variables, you can specify them here
      
      # Solve the optimization problem
      res = minimize(objective, self.u0, method='SLSQP', constraints={'type': 'ineq', 'fun': constraint}, bounds=bounds)
      
      if res.success:
        u_proj = res.x
        return u_proj
      else:
        raise ValueError("Optimization failed. Could not find the Bergman projection.")

  # Define the distance function
  def f(self,u):
      return np.linalg.norm(u)**2

  # Define the CBF constraint function (inequality)
  def g(self,u):
      return (u[0]*self.Lg_h[0][0] + self.Lg_h[0][1]*u[1] + self.h)

  # Define the gradient function for 2 norm
  def grad(self,p):
      p = np.array(p)
      # Gradient of y**2 is 2*y
      return 2*p

  '''# Define the gradient function
  def grad(self,p):
      #print('the input is = ', p)
      p = np.array(p)
      # Calculate the partial derivatives using finite differences
      f_p_plus = self.f(p+self.d)
      f_p_minus = self.f(p-self.d)

      df_dx = f_p_plus - f_p_minus
      df_dx = [df_dx/(2*self.d), df_dx/(2*self.d)]
      return df_dx'''
