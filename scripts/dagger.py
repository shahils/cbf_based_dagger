#!/usr/bin/env python3

#import tensorflow as tf
import os
import time
import tensorflow.compat.v1 as tf1
import numpy as np

#####################  hyper parameters  ####################

MAX_EPISODES = 10
n_iteration = 10
#n_iteration = 1


tf1.compat.v1.disable_eager_execution()
print('before')
print(tf1.config.list_physical_devices('GPU'))
print('after')
physical_devices = tf1.config.list_physical_devices('GPU') 
for device in physical_devices:
	tf1.config.experimental.set_memory_growth(device, True)

class Human_model(object):
	def __init__(self):
		super(Human_model, self).__init__()
		self.sess = tf1.Session()
		self.S1 = tf1.placeholder(tf1.float32, [None, 5], 'S1')
		self.S2 = tf1.placeholder(tf1.float32, [None, 724], 'S2')
		self.a_real = tf1.placeholder(tf1.float32, [None, 2], 'a_real')
		self.keep_prob = tf1.placeholder(tf1.float32)
		self.a_predict = self.build_c(self.S1,self.S2,self.keep_prob)
		#self.visuavalize = self.build_c2(self.S1,self.S2,self.keep_prob)
		self.loss = tf1.losses.mean_squared_error(labels=self.a_real, predictions=self.a_predict)
		self.train = tf1.train.AdamOptimizer(0.0002).minimize(self.loss)
		self.loader()
		self.initsess = tf1.global_variables_initializer()

	#####################  Net Work Building  ###################
	def build_c(self,S1,S2,keep_prob):
		
		net1 = tf1.layers.dense(S2, 1024, activation=tf1.nn.relu, name='net1', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.003))
		#drop_out1 = tf1.nn.dropout(net1, keep_prob)
		net2 = tf1.layers.dense(net1, 1024, activation=tf1.nn.relu, name='net2', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.003))
		#drop_out2 = tf1.nn.dropout(net2, keep_prob)
		net3 = tf1.layers.dense(net2, 512, activation=tf1.nn.relu, name='net3', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.003))
		#drop_out3 = tf1.nn.dropout(net3, keep_prob)
		net4 = tf1.layers.dense(net3, 512, activation=tf1.nn.relu, name='net4', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.003))
		#drop_out4 = tf1.nn.dropout(net4, keep_prob)
		net5 = tf1.layers.dense(net4, 256, activation=tf1.nn.relu, name='net5', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2action_space(0.003))
		#drop_out5 = tf1.nn.dropout(net5, keep_prob)
		net6 = tf1.layers.dense(net5, 128, activation=tf1.nn.relu, name='net6', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.003))
		drop_out6 = tf1.nn.dropout(net6, keep_prob)
		net7 = tf1.layers.dense(drop_out6, 64,trainable=True,activation=tf1.nn.relu, name='net7', kernel_regularizer=tf1.keras.regularizers.L2(0.003))
		#drop_out7 = tf1.nn.dropout(net7, keep_prob)
		
		net8 = tf1.layers.dense(S1, 256, activation=tf1.nn.relu, name='net8', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.003))
		#drop_out8 = tf1.nn.dropout(net8, keep_prob)
		net9 = tf1.layers.dense(net8,256, activation=tf1.nn.relu,name='net9',trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.003))
		#drop_out9 = tf1.nn.dropout(net9, keep_prob)
		net11 = tf1.layers.dense(net9,64, activation=tf1.nn.relu,name='net11',trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.003))
		#drop_out11 = tf1.nn.dropout(net11, keep_prob)

		net13_input = tf1.concat([net11, net7], 1)
		net13 = tf1.layers.dense(net13_input, 256, activation=tf1.nn.relu, name='net13', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.003))
		net14 = tf1.layers.dense(net13, 128, activation=tf1.nn.relu, name='net14', trainable=True, kernel_regularizer=tf1.keras.regularizers.L2(0.003))
	
		a = tf1.layers.dense(net14,2,trainable=True,activation='tanh')
		#a = tf1.layers.dense(net12,2,trainable=True,activation=tf1.tanh)
		#print('output = ', a)
		return tf1.multiply(a, [2,1], name='scaled_a')


		
	def train_net(self,j):
		
		self.data = np.loadtxt('pure_human_ros_full.dat')             #loading the data set which includes lidar data
		
		
		train_size = round(0.6*self.data.shape[0])
		print('training iteration number: ', j)
		print('train size = ', train_size)
		
		self.train_set_x = self.data[:train_size,:-2]
		self.train_set_y = self.data[:train_size,-2:]
		
		self.test_set_x = self.data[train_size:,:-2]
		self.test_set_y = self.data[train_size:,-2:]
		
		#print('data_test:',self.test_set.shape)
		MEMORY_CAPACITY = self.train_set_x.shape[0]
		MEMORY_CAPACITY_TEST = self.test_set_x.shape[0]
		BATCH_SIZE = 512
		self.MSE = 1
		for i in range(10000):
			index = np.random.choice(MEMORY_CAPACITY, size=BATCH_SIZE)
			index_test = np.random.choice(MEMORY_CAPACITY_TEST, size=BATCH_SIZE)
			
			train_set_robot = self.train_set_x[index,:5]
			train_set_lidar = self.train_set_x[index,5:]
			#bs2 = bs2.reshape([-1,198,1])
			train_set_output = self.train_set_y[index,:]
			train_set_output[:,0] = train_set_output[:,0]
			
			test_set_robot = self.test_set_x[:,:5]         #[index_test,:]
			test_set_lidar = self.test_set_x[:,5:]
			test_set_output = self.test_set_y
			test_set_output[:,0] = test_set_output[:,0]
			#bt2 = bt2.reshape([-1,198,1])
			#bt_y = text_set_y       #[index_test,:]

			self.sess.run(self.train,feed_dict={self.S1:train_set_robot, self.S2:train_set_lidar, self.a_real:train_set_output, self.keep_prob:0.8})
			if i % 100 == 0:
				cost = self.sess.run(self.loss,feed_dict={self.S1:test_set_robot, self.S2:test_set_lidar, self.a_real:test_set_output, self.keep_prob:1})
				#costlist.append(cost)
				print("after %i iteration, MSE: %f" %(i, cost))
				
				if cost < self.MSE:
					self.MSE = cost
					
	def loader(self):
		loader= tf1.train.Saver()
		loader.restore(self.sess,tf1.train.latest_checkpoint('Human_model_ros_full'))

	def saver(self,j):
		saver = tf1.train.Saver()
		#saver.save(self.sess,"Human_model_exp_2"+str(j)+"/net")
		saver.save(self.sess,"Human_model_ros_full"+"/net")
		print("*****net_saved******")


def main():

	Human = Human_model()
	success_rate = np.zeros([1,n_iteration])
	MSE = np.zeros([10,n_iteration])
	time_data = np.zeros([MAX_EPISODES,n_iteration])
	for i in range(1):
		Human.sess.run(Human.initsess)
		for j in range(n_iteration):
			Human.loader()
			Human.train_net(j)
			Human.saver(j)
			MSE[i,j]=Human.MSE
			np.savetxt("MSE3.dat",MSE)

if __name__ == '__main__':
	main()

