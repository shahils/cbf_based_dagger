# CBF_based_DAgger

## Dependencies:
Here is a list of dependecies you will need to source install/build:
1) ROS "common_msgs": https://github.com/ros/common_msgs
2) Velodyne ROS drivers: https://github.com/ros-drivers/velodyne
3) Warthog ROS drivers: https://github.com/warthog-cpr/warthog
4) ROS-TCP-Endpoint drivers: https://github.com/Unity-Technologies/ROS-TCP-Endpoint
5) ROS-TCP-Connector drivers: https://github.com/Unity-Technologies/ROS-TCP-Connector


## Getting started:
1) Install the list of dependencies mentioned above.
2) Place the "WartySimulation-TCP" folder in desired location and build the Unity3D simulation.
3) Create your workspace, clone the "launch" and "script" folder into "src" and then "catkin_make"

## Running the code:
1) Start the Unity3D simulation.
2) Source your workspace and launch ROS-TCP-Endpoint using "roslaunch ros_tcp_endpoint endpoint.launch". This will establis the ROS and Unity3D communication.
3) To get the 3D pointcloud data, launch the test launch file from the velodyne_pointcloud package sung the command "roslaunch velodyne_pointcloud test.launch".
4) Convert the 3D pointcloud data to 2D LaserScan by running "roslaunch my_package point_to_laser.launch".
5) Connect your JoyStick to ROS by running "rosrun joy joy_node".
6) Navigate to the "scripts" folder and run the "KeyToJoy.py" file to convert joystick commands to "cmd_vel".


## Visualization:
Utilize RVIZ for visualization. Subscribe to the "polygon_front" and "polygon_rear" messages to visualize the safe space.
