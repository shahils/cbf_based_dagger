using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RosMessageTypes.Nav;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.UrdfImporter.Control;
using static RosUnityTransforms;

public class HuskyPosePublisher : MonoBehaviour
{   
    public Transform tf;
    public Vector3 robotPoseUnity;
    public Vector3 robotPoseRos;
    public Rigidbody rb;
    private OdometryMsg odomMsg;
    public string frameId = "base_link";
    public string topicName = "unity_odom_info";

    ROSConnection ros;
    void Start()
    {
        ros = ROSConnection.GetOrCreateInstance();
        ros.RegisterPublisher<OdometryMsg>(topicName);
        odomMsg = new OdometryMsg();
        odomMsg.header.frame_id = frameId;
    }

    void Update()
    {   // Coverting Unity coordinates to ROS
        robotPoseUnity = tf.position;
        robotPoseRos = RosUnityTransforms.Unity2Ros(robotPoseUnity);

        odomMsg.pose.pose.position.x = robotPoseRos.x;
        odomMsg.pose.pose.position.y = robotPoseRos.y;
        odomMsg.pose.pose.position.z = robotPoseRos.z;

        /*odomMsg.pose.pose.position.x = tf.position.z;
        odomMsg.pose.pose.position.y = -tf.position.x;
        odomMsg.pose.pose.position.z = tf.position.y;*/


        Vector3 vel = RosUnityTransforms.Unity2Ros(tf.InverseTransformDirection(rb.velocity));  

        odomMsg.twist.twist.linear.x = vel[0];
        odomMsg.twist.twist.linear.y = vel[1];
        odomMsg.twist.twist.linear.z = vel[2];      
        
        /*odomMsg.twist.twist.linear.x = rb.velocity.z;
        odomMsg.twist.twist.linear.y = -rb.velocity.x;
        odomMsg.twist.twist.linear.z = rb.velocity.y;*/

        Vector3 angularVel = RosUnityTransforms.Unity2Ros(rb.angularVelocity);
        
        odomMsg.twist.twist.angular.x = angularVel.x;
        odomMsg.twist.twist.angular.y = angularVel.y;
        odomMsg.twist.twist.angular.z = angularVel.z;
        
        Quaternion rotation = RosUnityTransforms.Unity2Ros(Quaternion.Euler(rb.rotation.x * Mathf.Rad2Deg, rb.rotation.y * Mathf.Rad2Deg, rb.rotation.z * Mathf.Rad2Deg));
        
        odomMsg.pose.pose.orientation.x = rotation.x;
        odomMsg.pose.pose.orientation.y = rotation.y;
        odomMsg.pose.pose.orientation.z = rotation.z;
        odomMsg.pose.pose.orientation.w = rotation.w;

        
        ros.Publish(topicName, odomMsg);
        
    }
}

