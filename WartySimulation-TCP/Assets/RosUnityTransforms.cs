using System;
using System.IO;
using UnityEngine;
using Object = UnityEngine.Object;

public static class RosUnityTransforms
{
    public static Vector3 Ros2Unity(this Vector3 vector3)
    {
        return new Vector3(-vector3.y, vector3.z, vector3.x);
    }

    public static Vector3 Unity2Ros(this Vector3 vector3)
    {
        return new Vector3(vector3.z, -vector3.x, vector3.y);
    }

    public static Vector3 Ros2UnityScale(this Vector3 vector3)
    {
        return new Vector3(vector3.y, vector3.z, vector3.x);
    }

    public static Vector3 Unity2RosScale(this Vector3 vector3)
    {
        return new Vector3(vector3.z, vector3.x, vector3.y);
    }

    public static Quaternion Ros2Unity(this Quaternion quaternion)
    {
        return new Quaternion(quaternion.y, -quaternion.z, -quaternion.x, quaternion.w);
    }

    public static Quaternion Unity2Ros(this Quaternion quaternion)
    {
        return new Quaternion(-quaternion.z, quaternion.x, -quaternion.y, quaternion.w);
    }

    /*public static double[] ToRoundedDoubleArray(this Vector3 vector3)
    {
        double[] arr = new double[3];
        for (int i = 0; i < 3; i++)
            arr[i] = Math.Round(vector3[i], RoundDigits);

        return arr;
    }*/

    public static Vector3 ToVector3(this double[] array)
    {
        return new Vector3((float)array[0], (float)array[1], (float)array[2]);
    }
}
