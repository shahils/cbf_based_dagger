using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RosMessageTypes.Nav;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.UrdfImporter.Control;

public class GoalPose : MonoBehaviour
{   
    public Transform tf;
    //public Rigidbody rb;
    private OdometryMsg goalMsg;
    public string frameId = "base_link";
    public string topicName = "goal_pose";

    ROSConnection ros;
    void Start()
    {
        ros = ROSConnection.GetOrCreateInstance();
        ros.RegisterPublisher<OdometryMsg>(topicName);
        goalMsg = new OdometryMsg();
        goalMsg.header.frame_id = frameId;
    }

    void Update()
    {   // Coverting Unity coordinates to ROS
        goalMsg.pose.pose.position.x = tf.position.z;
        goalMsg.pose.pose.position.y = -tf.position.x;
        goalMsg.pose.pose.position.z = tf.position.y;
        
        ros.Publish(topicName, goalMsg);
        
    }
}

