using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RosMessageTypes.Geometry;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.UrdfImporter.Control;

namespace RosSharp.RosBridgeClients
{
    public class HuskyController_ : MonoBehaviour
    {
        [Header("Wheel Collider Objects")]
        public WheelCollider front_left_wheel_joint;
        public WheelCollider front_right_wheel_joint;
        public WheelCollider rear_left_wheel_joint;
        public WheelCollider rear_right_wheel_joint;
        
        [Header("Wheel Transform Objects")]
        public Transform WheelFrontLeftTransform;
        public Transform WheelFrontRightTransform;
        public Transform WheelRearLeftTransform;
        public Transform WheelRearRightTransform;

        [Header("Clearpath Husky Dimensions")]
        public float maxMotorTorque = 50.0f;
        public float minMotorTorque = 3.0f;
        public float minMotorSpeed = 0.001f; 
        public float brakeTorque = 20f;
        public float kp = 20f;
        public float kd = 0;
        public float ki = 5.0f;
        public float iClamp = 5.0f;
        private float roslinear = 0.0f;
        private float rosangular = 0.0f;
        private float wheelRadius= 0.165f; 
        private float wheelBase = 0.544f; 
        private float desired_left_speed = 0.0f;
        private float desired_right_speed = 0.0f;

        float alpha = 0.25f;
        float[] average_speed = {0, 0, 0, 0};
        float[] i_error = {0, 0, 0, 0};
        float[] p_error_last = {0, 0, 0, 0};
        
        [Header("Parameters")]
        public float velocityMultiplier = 1.0f; // Propotional scaling for velocity to torque mapping
        private float vel_left = 0.0f;
        private float vel_right = 0.0f;
        //private float roslinear = 0.0f;
        //private float rosangular = 0.0f;

        ROSConnection ros; // Instantiating ROSConnection object to establish connection with ros

        void Start()
        {   
            ros = ROSConnection.GetOrCreateInstance();
            ros.Subscribe<TwistMsg>("cmd_vel", ROSCommand);
        }

        void FixedUpdate()
        {
                MoveHusky();
        }

        void ROSCommand(TwistMsg ros_cmd_vel) 
        {
            roslinear = (float)ros_cmd_vel.linear.x;
            rosangular = (float)ros_cmd_vel.angular.z;
            desired_right_speed = ((2*roslinear + rosangular*wheelBase)/(2*wheelRadius));
            desired_left_speed = ((2*roslinear - rosangular*wheelBase)/(2*wheelRadius));
            //Debug.Log("Error: " + desired_left_speed);
        }

        void MoveHusky() {

            float input = Input.GetAxis("Vertical");
            
            int i = 0;
            float speed = front_left_wheel_joint.rpm * 1/60.0f * 2.0f * Mathf.PI;
            average_speed[i] = (1.0f - alpha) * average_speed[i] + alpha * speed;
            front_left_wheel_joint.motorTorque = ClampTorque(SpeedControl(desired_left_speed, average_speed[i], i, Time.deltaTime));

            i = 1;
            speed = front_right_wheel_joint.rpm * 1/60.0f * 2.0f * Mathf.PI;
            average_speed[i] = (1.0f - alpha) * average_speed[i] + alpha * speed;
            front_right_wheel_joint.motorTorque = ClampTorque(SpeedControl(desired_right_speed, average_speed[i], i, Time.deltaTime));

            i = 2;
            speed = rear_left_wheel_joint.rpm * 1/60.0f * 2.0f * Mathf.PI;
            average_speed[i] = (1.0f - alpha) * average_speed[i] + alpha * speed;
            rear_left_wheel_joint.motorTorque = ClampTorque(SpeedControl(desired_left_speed, average_speed[i], i, Time.deltaTime));

            i = 3;
            speed = rear_right_wheel_joint.rpm * 1/60.0f * 2.0f * Mathf.PI;
            average_speed[i] = (1.0f - alpha) * average_speed[i] + alpha * speed;
            rear_right_wheel_joint.motorTorque = ClampTorque(SpeedControl(desired_right_speed, average_speed[i], i, Time.deltaTime));


            WheelFrontLeftTransform.Rotate(front_left_wheel_joint.rpm, 0, 0); 
            WheelFrontRightTransform.Rotate(front_right_wheel_joint.rpm, 0, 0);
            WheelRearLeftTransform.Rotate(rear_left_wheel_joint.rpm, 0, 0);
            WheelRearRightTransform.Rotate(rear_right_wheel_joint.rpm, 0, 0);

        }
    

        public float SpeedControl(float speed_setpoint, float speed_value, int index, float dt)
        {
            float error = speed_setpoint - speed_value;

            // if(Mathf.Abs(error) < 0.1f)
            //     return 0.0f;

            float error_dot = 0;
            if(dt > 0) {
                error_dot = (error - p_error_last[index]) / dt;
                p_error_last[index] = error;
            }

            if(ki > 0) {
                i_error[index] += dt * error;
            
                float i_term = ki * i_error[index];
            
                if(i_term > iClamp) {
                    i_error[index] = iClamp/ki;
                }
                else if(i_term < -iClamp) {
                    i_error[index] = -iClamp/ki;
                }
            }

                
            float desired_torque = kp * error + ki * i_error[index] + kd * error_dot;

            if(index == 0) {
                Debug.Log("Error: " + desired_left_speed + ", " + i_error[index] + ", " + error_dot + "; Terms: " + (kp * error) + ", " + (ki * i_error[index]) + ", " + (kd*error_dot) + " => " + desired_torque);
            }
            
            if(Math.Abs(desired_torque) < minMotorTorque) {
                i_error[index] = 0;
                return 0;
            }
            return desired_torque;
        }

        float ClampTorque(float desired_torque)
        {
            float torque_clamped = desired_torque > 0 ? Math.Min(desired_torque, maxMotorTorque) : Math.Max(desired_torque, -maxMotorTorque);

            return torque_clamped;
            // return Math.Abs(torque_clamped) < 0.1f ? 0.0f : torque_clamped;
        }

    }
}
