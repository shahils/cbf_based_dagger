using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RosMessageTypes.Geometry;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.UrdfImporter.Control;

namespace RosSharp.RosBridgeClients
{
    public class WarthhogController_ : MonoBehaviour
    {
        [Header("Wheel Collider Objects")]
        public WheelCollider WheelFrontLeftCollider;
        public WheelCollider WheelFrontRightCollider;
        public WheelCollider WheelRearLeftCollider;
        public WheelCollider WheelRearRightCollider;
        
        [Header("Clearpath Warthhog Dimensions")]
        private float wheelRadius= 0.165f; 
        private float wheelBase = 0.544f; 
        
        [Header("Parameters")]
        public float velocityMultiplier = 0.5f; // Propotional scaling for velocity to torque mapping
        private float vel_left = 0.0f;
        private float vel_right = 0.0f;
        private float roslinear = 0.0f;
        private float rosangular = 0.0f;

        ROSConnection ros; // Instantiating ROSConnection object to establish connection with ros

        void Start()
        {   
            ros = ROSConnection.GetOrCreateInstance();
            ros.Subscribe<TwistMsg>("cmd_vel", ROSCommand);
        }

        void FixedUpdate()
        {
                MoveWarthhog();
        }

        void ROSCommand(TwistMsg ros_cmd_vel)
        {
            roslinear = (float)ros_cmd_vel.linear.x;
            rosangular = (float)ros_cmd_vel.angular.z;
            vel_right = 180/Mathf.PI*((2*roslinear + rosangular*wheelBase)/(2*wheelRadius));
            vel_left = 180/Mathf.PI*((2*roslinear - rosangular*wheelBase)/(2*wheelRadius));
        }

        void MoveWarthhog()
        {
            // Updating wheel velocity
            WheelFrontLeftCollider.motorTorque = velocityMultiplier*vel_left;
            WheelFrontRightCollider.motorTorque = velocityMultiplier*vel_right;
            WheelRearLeftCollider.motorTorque = velocityMultiplier*vel_left;
            WheelRearRightCollider.motorTorque = velocityMultiplier*vel_right;
            
            WheelFrontLeftCollider.transform.Rotate(-WheelFrontLeftCollider.rpm, 0, 0); 
            WheelFrontRightCollider.transform.Rotate(-WheelFrontRightCollider.rpm, 0, 0);
            WheelRearLeftCollider.transform.Rotate(-WheelRearLeftCollider.rpm, 0, 0);
            WheelRearRightCollider.transform.Rotate(-WheelRearRightCollider.rpm, 0, 0); 
        }
    }
}