using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector;  // Import the ROS TCP Connector package
using RosMessageTypes.Std;

public class Spawn_Warty : MonoBehaviour
{
    public GameObject cube;
    public GameObject warty;
    public double xdistance;
    public double zdistance;
    public Vector3 cubePosition;
    public Vector3 watyPosition; 
    public Transform wartySpawn; // Reference to the robot's Transform component
    public Vector3 newWartyPosition; // The position where you want to place the robot
    public Vector3[,] treePositions;
    public double xmin = 471;
    public double xmax = 508;
    public double zmin = 423;
    public double zmax = 476;
    public Rigidbody robotRigidbody;

    private ROSConnection ros;  // ROS connection

    /*public float[,] goalMatrix = new float[3,4]{
        {503.17f, 478.13f, 474.13f, 498f},
        {17.11f, 17.27f, 16.86f, 17.16f},
        {473.69f, 460.71f, 446.07f, 432.17f}
    };*/

    public void Start()
    {
        // Initialize ROS connection
        ros = ROSConnection.GetOrCreateInstance();

        // Add subscriber registration here
        ros.Subscribe<BoolMsg>("done_topic", checkWartySpawn);

        // Add your existing Start code here
        // cube = GameObject.Find("Cube");
        // warty = GameObject.Find("Warthog");
    }

    public void Update()
    {
        cube = GameObject.Find("Cube");
        warty = GameObject.Find("Warthog");

        // Obstacle 
        GameObject[] treeObjects = GameObject.FindGameObjectsWithTag("tree");
        treePositions = new Vector3[1,treeObjects.Length];
        
        // Loop through the tree objects and store their position in the matrix
        for (int i = 0; i < treeObjects.Length; i++)
        {
            treePositions[0,i] = treeObjects[i].transform.position;
        }

        //Vector3 temp =  positions[0,1];
        //Debug.Log("Tree position: " + temp[0]);

        cubePosition = cube.transform.position;
        watyPosition = warty.transform.position;
        
        //DistanceToGoal();
    }

    public void checkWartySpawn(BoolMsg message)
    {
        if (message.data)
        {
            SpawnWarty();
            SetRobotVelocity(Vector3.zero);
        }
    }

    private void SetRobotVelocity(Vector3 velocity)
    {
        // Update the robot's rigidbody velocity to zero
        robotRigidbody.velocity = velocity;
    }

    public void SpawnWarty()
    {
        // Set the Goals's position to the desired location
        System.Random randX = new System.Random();
        System.Random randZ = new System.Random();
        double xpose = randX.NextDouble() * (xmax-xmin) + xmin;
        double zpose = randZ.NextDouble() * (zmax-zmin) + zmin;
        Vector3 obs;
        int valid = 1;

        newWartyPosition[0] = (float)xpose;
        newWartyPosition[1] = 17f;
        newWartyPosition[2] = (float)zpose;

        for (int i = 0; i < treePositions.Length; i++)
        {
            obs = treePositions[0,i];
            if (Math.Abs(newWartyPosition[0] - obs[0]) < 3 && Math.Abs(newWartyPosition[2] - obs[2]) < 3)
            {
                valid = valid*0;
            }
            else
            {
                valid = valid*1;
            }
        }

        if (valid == 1)
        {
            wartySpawn.position = newWartyPosition;
        }
        
    }
}

