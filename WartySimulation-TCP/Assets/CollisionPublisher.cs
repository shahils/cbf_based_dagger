using UnityEngine;
using RosMessageTypes.Std; // Corrected import statement
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.UrdfImporter.Control;

public class CollisionPublisher : MonoBehaviour
{
    public string rosTopic = "collision_detected";
    ROSConnection rosConnection;
    private bool collisionDetected = false;
    
    private void Start()
    {
        // Initialize ROS connection
        rosConnection = ROSConnection.GetOrCreateInstance();
        // rosConnection.OnRosBridgeConnected += OnRosConnected;

        // Register a subscriber to detect collisions
        rosConnection.RegisterPublisher<BoolMsg>(rosTopic); // Corrected message type
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Check if the collision occurred with the robot
        BoolMsg collisionMsg = new BoolMsg(true);
        rosConnection.Publish(rosTopic, collisionMsg);
    }

}
